#!/bin/sh

VERSION="${1}"
if [[ "${VERSION}" == "" ]]; then
    echo "Version is required."
    exit 1
fi

MODIFICATIONS=`git status --short --untracked-files | wc -l`
if [[ ${MODIFICATIONS} != 0 ]]; then
    >&2 echo "Unsaved modifications found - please commit all changes before releasing."
    exit 1
fi

COMMITS_AHEAD=`git rev-list @{u}.. | wc -l`
if [[ ${COMMITS_AHEAD} != 0 ]]; then
    >&2 echo "Unpushed commits exist in workspace - please push all changes before releasing."
    exit 1
fi

sed --in-place "s/version='.*'/version='${VERSION}'/" setup.py
if [[ $? != 0 ]]; then
    echo "Failed replacing version in setup.py"
    exit 1
fi

git add setup.py && git commit -m "Update version to ${VERSION}"
if [[ $? != 0 ]]; then
    echo "Failed committing version change"
    exit 1
fi

git tag -a "${VERSION}" -m "Version ${VERSION}"
if [[ $? != 0 ]]; then
    echo "Failed tagging version"
    exit 1
fi

git push origin "${VERSION}"
if [[ $? != 0 ]]; then
    echo "Failed tagging version"
    exit 1
fi

rm -rf build dist

python setup.py sdist bdist_wheel
if [[ $? != 0 ]]; then
    echo "Failed building release"
    exit 1
fi

twine upload --repository pypi dist/*
if [[ $? != 0 ]]; then
    echo "Failed uploading release"
    exit 1
fi

sed --in-place "s/version='.*'/version='dev'/" setup.py
if [[ $? != 0 ]]; then
    echo "Failed reverting version back to 'dev' in setup.py"
    exit 1
fi

git add setup.py && git commit -m "Revert version back to 'dev'"
if [[ $? != 0 ]]; then
    echo "Failed committing version change back to 'dev'"
    exit 1
fi

git push
if [[ $? != 0 ]]; then
    echo "Failed pushing version revert back to 'dev'"
    exit 1
fi
